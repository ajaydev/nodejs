const express = require('express');
const cors = require('cors');
require('dotenv').config({path: __dirname + '/.env'})

const app = express();
app.use(cors());

app.use('/movie', require('./routes/movie'));

app.listen(process.env.PORT);
console.log("server listen on "+process.env.PORT+" port.");

