const auth = require('basic-auth');
const utils = require('../utilities/utils');

function verifyUser(req, res, next) {
    let credentials = auth(req);
    if (credentials && credentials.name === process.env.BASE_AUTH_USERNAME && credentials.pass === process.env.BASE_AUTH_PASSWORD)
        next();
    else
        return utils.sendResponse(res, 401, 'Basic auth is not provided', { });
}

module.exports = { verifyUser }
