const express = require('express');
const router = express.Router();

const basicAuth = require('../authorize/basic_auth');
const utils = require('../utilities/utils');
const movieController = require('../controllers/movie');

router.get('/', basicAuth.verifyUser, getMovieDetails);
router.get('/list', basicAuth.verifyUser, getMoviesList);

function getMoviesList(req, res){
    if(typeof req.query.search != "string" || req.query.search.replace(/ /g,'').length == 0 || typeof req.query.page != "string" || req.query.page.replace(/ /g,'').length == 0 || isNaN(parseInt(req.query.page.replace(/ /g,''))))
        return utils.sendResponse(res, 422, 'Required Parameter Missing or Invalid', { });
    
    movieController.getMoviesList(req.query).then(response => {
        return utils.sendResponse(res, response.status, response.msg, response.data);
    }).catch(error => {
        if(error.status && error.msg)
            return utils.sendResponse(res, error.status, error.msg, error.data);
        else
            return utils.sendResponse(res, 500, 'Error while getting movie details', { });
    });
}

function getMovieDetails(req, res){
    if(typeof req.query.imdbID != "string" || req.query.imdbID.replace(/ /g,'').length == 0)
        return utils.sendResponse(res, 422, 'Required Parameter Missing or Invalid', { });
    
    movieController.getMovieDetails(req.query).then(response => {
        return utils.sendResponse(res, response.status, response.msg, response.data);
    }).catch(error => {
        if(error.status && error.msg)
            return utils.sendResponse(res, error.status, error.msg, error.data);
        else
            return utils.sendResponse(res, 500, 'Error while getting movie details', { });
    });
}

module.exports = router
