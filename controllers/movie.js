const axios = require('axios');
const omdbApiKey = process.env.OMDB_API_KEY;

const constants = require('../utilities/constants.json');

function getMoviesList(payload) {
    return new Promise((resolve, reject) => {
        try {
            const search = payload.search;
            const page = payload.page.replace(/ /g,'');
            let url = `http://www.omdbapi.com?apikey=${omdbApiKey}&s=${search}&page=${page}`;
            (typeof payload.type == "string" && payload.type.replace(/ /g,'').length > 0 && (payload.type == "0" || payload.type == "1" || payload.type == "2")) ? url += `&type=${constants.TYPES[payload.type]}` : null;
            (typeof payload.year == "string" && payload.year.replace(/ /g,'').length > 0 && !isNaN(parseInt(payload.year))) ? url += `&y=${payload.year}` : null;
            axios.get(url).then(function (response) {
                const moviesList = (response.data.Search) ? response.data.Search : [];
                const totalResults = (response.data.totalResults) ? response.data.totalResults : 0;
                return resolve({ status: 200, msg: 'Movies list get successfully', data: {moviesList, totalResults} });
            }).catch(function (error) {
                return reject(error);
            })
        } catch (error) {
            return reject(error);
        }
    })
}

function getMovieDetails(payload) {
    return new Promise((resolve, reject) => {
        try {
            const imdbID = payload.imdbID.replace(/ /g,'');
            axios.get(`http://www.omdbapi.com?apikey=${omdbApiKey}&i=${imdbID}&plot=full`).then(function (response) {
                const res = {
                    imdbID: response.data.imdbID,
                    Title: response.data.Title,
                    Poster: response.data.Poster,
                    Year: response.data.Year,
                    Type: response.data.Type,
                    Released: response.data.Released,
                    Genre: response.data.Genre,
                    Ratings: response.data.Ratings
                };
                return resolve({ status: 200, msg: 'Movie details get successfully', data: res });
            }).catch(function (error) {
                return reject(error);
            })
        } catch (error) {
            return reject(error);
        }
    })
}

module.exports = {
    getMoviesList,
    getMovieDetails
}